# Liedzettel

**Einzug und Orgelvorspiel** Largo (Händel) 
*/ Präludium g-Moll (Bach) / Brautchor aus Lohengrin (Wagner) / Kanon in D-Dur (Pachelbel) / ...*

**Lied** Alles meinem Gott zu ehren  

**Begrüßung**

**Kyrie** `gesungen oder gesprochen` Meine engen Grenzen

**Gloria** `optional, gesungen oder gesprochen` Laudate omnes gentes */ Lobe den Herrn* 

**Tagesgebet**

**1. Lesung** noch auszusuchen */ Alles hat seine Zeit Koh 3,1-12 / ...*

**Zwischengesang** Da berühren sich Himmel und Erde 

**2. Lesung** noch auszusuchen */ [„Muscheln in der Hand“](https://www.ein-unvergesslicher-tag.de/lesungen-zur-hochzeit/muscheln-in-meiner-hand/) / Röm 12,10-17 / ...*

**Halleluja** noch auszusuchen / Ruf mit Vers?

**Evangelium** noch auszusuchen */ Lk 12, 22-31(-34)*

**Predigt**

**Credo** `optional` Glaubensbekenntnis / Fest soll mein Taufbund stehen

**Trauung** `Vero/Franz/Daniel` From a distance */ Herr vor dein Antlitz treten zwei, um künftig eins zu sein*

`ggf. nach Trauungssegen Gottes guter Segen sei mit euch / Instrumentalstück / ...`

**Fürbitten**

**Lied zur Gabenbereitung** noch auszusuchen */ Herr, wir bringen in Brot und Wein / instrumentell / ...*

**Hochgebet**

**Sanctus** noch auszusuchen */ Heilig, heilig, heilig, heilig ist der Herr / ...*

**Vaterunser**

**Friedensgruß**

**Agnus Dei** `gesungen oder gesprochen` Lamm Gottes */ dona nobis pacem*

**Kommunionausteilung** `Vero/Franz/Daniel` Laudate Dominum (Mozart)

**Danklied** Segne du Maria */ Nun danket alle Gott*

**Schlussgebet und Segen**

**Schlusslied** Großer Gott wir loben dich

**Auszug** noch auszusuchen */ Toccata F-Dur (Bach) / Toccata f-Moll Midi (J.M.Widor) / Bayernhymne / Improvisation*